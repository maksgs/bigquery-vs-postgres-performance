--calculates population by country and year, filter data by year
with population_by_year_and_location as(
select
	sum (value) as value,
	Year,
	location_name
from
	populations_aggregated a
where
	location_type = 'COUNTRY'
group by 2, 3),

--calculates diffs per country, year
diff as (
select
	a.value - b.value as diff_value,
	a.year,
	a.location_name
from
	population_by_year_and_location a
	inner join population_by_year_and_location b on a.location_name = b.location_name and a.year = b.year - 1
)

--select top 5 oldest countries per each year
SELECT
  *
FROM (
  SELECT
    ROW_NUMBER() OVER (PARTITION BY Year ORDER BY diff_value desc) AS row_num,
    t.*
  FROM
    diff t) temp
WHERE
  temp.row_num <= 5
order by
	Year desc, diff_value desc

