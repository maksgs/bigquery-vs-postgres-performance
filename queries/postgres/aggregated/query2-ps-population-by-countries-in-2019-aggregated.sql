select 
	sum (value), 
	location_name 
from 
	populations_aggregated a
where
	year =  2019 and location_type = 'COUNTRY'
group by 2
order by 1 desc