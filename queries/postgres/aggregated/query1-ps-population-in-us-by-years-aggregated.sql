select
    sum (value),
    year
from
    populations_aggregated
where
    location_name = 'United States of America'
group by
    2
order by
    year asc