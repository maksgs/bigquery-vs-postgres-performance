--converts age to number
with population_by_age_year_and_location as(
select
	sum (value) as value,
	Year,
	regexp_replace(age_group_name, '\+', '')::int as age,
	location_name
from
	populations a
	inner join locations b on a.location_id = b.id
	inner join age_groups c on a.age_group_id = c.id
where
	location_type = 'COUNTRY'
group by 2,3,4),

--calculates how many old people are in each country per year
old_population_by_year_and_locations as (
select
	sum(value) as value,
	Year,
	location_name
from
	population_by_age_year_and_location
WHERE
	age >= 60
group by
	2, 3
),
--calculates total number of people in country per year
total_population_by_year_and_locations as (
select
	sum(value) as value,
	Year,
	location_name
from
	population_by_age_year_and_location
group by
	2, 3
),
--calculates percent of old people per country per year
percent_of_old_population as (
select
	a.value / b.value * 100 as old_people_percent,
	a.Year,
	a.location_name
from
	old_population_by_year_and_locations a
	inner join total_population_by_year_and_locations b on a.year = b.year and a.location_name = b.location_name
)

--select top 5 oldest countries per each year
SELECT
  *
FROM (
  SELECT
    ROW_NUMBER() OVER (PARTITION BY Year ORDER BY old_people_percent desc) AS row_num,
    t.*
  FROM
    percent_of_old_population t) temp
WHERE
  temp.row_num <= 5
order by
	Year desc, old_people_percent desc
