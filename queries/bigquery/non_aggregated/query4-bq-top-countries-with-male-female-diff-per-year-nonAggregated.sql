--groups data by sex,country and year
with population_by_sex_year_and_location as(
select
	sum (value) as value,
	Year,
	sex,
	location_name
from
	world_population.populations a
	inner join world_population.locations b on a.location_id = b.id
where
	location_type = 'COUNTRY'
group by 2,3,4),

--calculates male and female population per country
male_female_population_by_year_and_locations as (
select
	a.value as male_population,
	b.value as female_population,
	a.Year,
	a.location_name
from
	population_by_sex_year_and_location a
	INNER JOIN population_by_sex_year_and_location b on a.year = b.year and a.location_name = b.location_name and a.sex = 'M' and b.sex = 'F'
),

--calculates diff between male and female per year and country
diff_population_by_year_and_locations as (
select
	male_population,
	female_population,
	male_population + female_population as total_population,
	abs(male_population - female_population ) / (male_population + female_population ) as diff,
	Year,
	location_name
from
	male_female_population_by_year_and_locations
)

--select top 5 oldest countries per each year
SELECT
  *
FROM (
  SELECT
    ROW_NUMBER() OVER (PARTITION BY Year ORDER BY diff desc) AS row_num,
    t.*
  FROM
    diff_population_by_year_and_locations t) temp
WHERE
  temp.row_num <= 5
order by
	Year desc, diff desc
