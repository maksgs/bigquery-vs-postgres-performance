select
	sum (value),
	year
from
	world_population.populations a
	inner join world_population.locations b on a.location_id = b.id
where
location_name = 'United States of America'
group by 2
order by year asc