select
	sum (value),
	location_name
from
	world_population.populations a
	inner join world_population.locations b on a.location_id = b.id
where
	year =  '2019' and location_type = 'COUNTRY'
group by 2
order by 1 desc