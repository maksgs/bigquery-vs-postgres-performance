--converts age to number
with population_by_age_year_and_location as(
select
	sum (value) as value,
	cast (regexp_replace(age_group_name, '\\+', '') as int64) as age,
	year,
	location_name
from
	world_population.populations_aggregated
where
	location_type = 'COUNTRY'
group by 2,3,4),

--calculates total population per country per year
total_population_by_year_and_locations as (
select
	sum(value) as value,
	year,
	location_name
from
	population_by_age_year_and_location
group by
	2,3
),

--calculates total number of people in country per year
age_multiplied_by_population_temp as (
select
	sum(value * age) as value,
	year,
	location_name
from
	population_by_age_year_and_location
group by
	2,3
),

median_per_year_country as (
select
	a.value / b.value as median,
	a.year,
	a.location_name
from
	age_multiplied_by_population_temp a
	inner join total_population_by_year_and_locations b on a.location_name = b.location_name and a.year = b.year
)

select * from median_per_year_country
order by year desc, median desc