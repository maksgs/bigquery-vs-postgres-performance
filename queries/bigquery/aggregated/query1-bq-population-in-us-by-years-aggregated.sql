select
    sum (value),
    year
from
    world_population.populations_aggregated
where
    location_name = 'United States of America'
group by
    2
order by
    year asc